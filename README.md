# gnome-commander

Graphical two-pane filemanager for Gnome

http://gcmd.github.io/

https://download.gnome.org/sources/gnome-commander/

<br>

How to clone this repository:

```
git clone https://gitlab.com/azul4/content/gnome/gnome-commander/gnome-commander.git
```

